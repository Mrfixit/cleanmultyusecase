package ru.stream.cleantest.domain.auth.entities.impl

import android.util.Log
import io.reactivex.Single
import ru.stream.cleantest.data.api.Api
import ru.stream.cleantest.domain.auth.entities.SendAuthDataEntity

class SendAuthDataEntityImpl(
        val api: Api
) : SendAuthDataEntity {
    override fun execute(login: String, pass: String, token: String): Single<Boolean> {
        return api.sendAuth(login, pass, token)
                .map({ it -> it.result })
                .doOnSuccess {
                    Log.d("MAGIC", "SendAuthDataEntityImpl received $it")
                }
    }
}