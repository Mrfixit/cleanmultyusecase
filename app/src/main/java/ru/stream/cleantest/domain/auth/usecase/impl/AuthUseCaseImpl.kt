package ru.stream.cleantest.domain.auth.usecase.impl

import io.reactivex.Single
import ru.stream.cleantest.domain.auth.entities.ReceiveTokenEntity
import ru.stream.cleantest.domain.auth.entities.SendAuthDataEntity
import ru.stream.cleantest.domain.auth.usecase.AuthUseCase
import ru.stream.cleantest.domain.entities.ObfuscateStringEntity

class AuthUseCaseImpl(
        private val receiveTokenEntity: ReceiveTokenEntity,
        private val sendAuthDataEntity: SendAuthDataEntity,
        private val obfuscateStringEntity: ObfuscateStringEntity
) : AuthUseCase {

    override fun execute(login: String, pass: String): Single<Boolean> {
        return receiveTokenEntity.execute()
                .flatMap { obfuscateStringEntity.execute(it) }
                .flatMap { sendAuthDataEntity.execute(login, pass, it) }
    }
}