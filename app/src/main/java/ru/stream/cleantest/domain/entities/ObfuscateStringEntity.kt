package ru.stream.cleantest.domain.entities

import io.reactivex.Single
import ru.stream.cleantest.domain.base.Entity

/**
 * Created by Daniil Belevtsev on 08.05.2018 11:40.
 * Project: CleanTest
 * Обфусцирует токен
 */
interface ObfuscateStringEntity : Entity {
    fun execute(inputString: String): Single<String>
}