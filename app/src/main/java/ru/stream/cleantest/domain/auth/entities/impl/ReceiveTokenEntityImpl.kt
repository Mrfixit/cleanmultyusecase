package ru.stream.cleantest.domain.auth.entities.impl

import android.util.Log
import io.reactivex.Single
import ru.stream.cleantest.data.repo.TokenRepository
import ru.stream.cleantest.domain.auth.entities.ReceiveTokenEntity

class ReceiveTokenEntityImpl(val tokenRepository: TokenRepository) : ReceiveTokenEntity {
    override fun execute(): Single<String> {
        return tokenRepository.getToken()
                .doOnSuccess {
                    Log.d("MAGIC", "ReceiveTokenEntity received $it")
                }
    }
}