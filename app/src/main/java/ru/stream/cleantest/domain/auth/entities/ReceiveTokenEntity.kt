package ru.stream.cleantest.domain.auth.entities

import io.reactivex.Single
import ru.stream.cleantest.domain.base.Entity

/**
 * Created by Daniil Belevtsev on 08.05.2018 11:26.
 * Project: CleanTest
 * Получает токен по deviceId телефона
 */
interface ReceiveTokenEntity : Entity {
    fun execute(): Single<String>
}