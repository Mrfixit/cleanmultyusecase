package ru.stream.cleantest.domain.auth.usecase

import io.reactivex.Single

/**
 * Created by Daniil Belevtsev on 08.05.2018 11:23.
 * Project: CleanTest
 */
interface AuthUseCase {
    fun execute(login: String, pass: String): Single<Boolean>
}