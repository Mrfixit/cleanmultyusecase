package ru.stream.cleantest.domain.entities

import android.util.Log
import io.reactivex.Single

class ObfuscateStringEntityImpl : ObfuscateStringEntity {
    override fun execute(inputString: String): Single<String> {
        return Single.just(StringBuilder(inputString).reverse().toString().toUpperCase())
                .doOnSuccess {
                    Log.d("MAGIC", "ObfuscateStringEntityImpl + $inputString obfuscated to $it")
                }
    }
}