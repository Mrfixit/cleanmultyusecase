package ru.stream.cleantest.domain.auth.entities

import io.reactivex.Single
import ru.stream.cleantest.domain.base.Entity

/**
 * Created by Daniil Belevtsev on 08.05.2018 11:41.
 * Project: CleanTest
 */
interface SendAuthDataEntity : Entity {
    fun execute(login: String, pass: String, token: String): Single<Boolean>
}