package ru.stream.cleantest.data.dto

/**
 * Created by Daniil Belevtsev on 08.05.2018 15:49.
 * Project: CleanTest
 */
data class ResponseDto(val result: Boolean) {
}