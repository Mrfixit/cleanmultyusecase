package ru.stream.cleantest.data.repo

import io.reactivex.Single

/**
 * Created by Daniil Belevtsev on 08.05.2018 15:45.
 * Project: CleanTest
 */
interface TokenRepository {
    fun getToken(): Single<String>
}