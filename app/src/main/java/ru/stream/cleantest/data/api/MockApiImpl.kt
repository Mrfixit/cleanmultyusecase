package ru.stream.cleantest.data.api

import io.reactivex.Single
import ru.stream.cleantest.data.dto.ResponseDto
import java.util.*

class MockApiImpl : Api {
    override fun sendAuth(login: String, pass: String, token: String): Single<ResponseDto> {
        return Single.just(ResponseDto(Random().nextBoolean()))
    }
}