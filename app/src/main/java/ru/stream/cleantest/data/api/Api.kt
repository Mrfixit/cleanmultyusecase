package ru.stream.cleantest.data.api

import io.reactivex.Single
import ru.stream.cleantest.data.dto.ResponseDto

/**
 * Created by Daniil Belevtsev on 08.05.2018 15:47.
 * Project: CleanTest
 */
interface Api {
    fun sendAuth(login: String, pass: String, token: String): Single<ResponseDto>
}