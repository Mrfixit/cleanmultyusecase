package ru.stream.cleantest.data.repo

import io.reactivex.Single
import java.util.*

class MockTokenRepositoryImpl : TokenRepository {
    override fun getToken(): Single<String> {
        return Single.just(UUID.randomUUID().toString().toLowerCase())
    }
}