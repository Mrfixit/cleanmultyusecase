package ru.stream.cleantest.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.activity_main.*
import ru.stream.cleantest.R
import ru.stream.cleantest.presentation.di.DiModule

class MainActivity : AppCompatActivity() {

    private val authUseCase = DiModule.provideAuthUseCase()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonAuth.setOnClickListener({
            authUseCase.execute("login", "password")
                    .subscribe(Consumer {
                        Toast.makeText(this, selectResult(it), Toast.LENGTH_SHORT).show()
                    })
        })
    }

    private fun selectResult(result: Boolean): String {
        if (result) {
            return "Чудо случилось"
        }
        return "Ничего не получилось!"
    }

}
