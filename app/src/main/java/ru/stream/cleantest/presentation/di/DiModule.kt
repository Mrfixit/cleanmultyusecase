package ru.stream.cleantest.presentation.di

import ru.stream.cleantest.data.api.Api
import ru.stream.cleantest.data.api.MockApiImpl
import ru.stream.cleantest.data.repo.MockTokenRepositoryImpl
import ru.stream.cleantest.data.repo.TokenRepository
import ru.stream.cleantest.domain.auth.entities.ReceiveTokenEntity
import ru.stream.cleantest.domain.auth.entities.SendAuthDataEntity
import ru.stream.cleantest.domain.auth.entities.impl.ReceiveTokenEntityImpl
import ru.stream.cleantest.domain.auth.entities.impl.SendAuthDataEntityImpl
import ru.stream.cleantest.domain.auth.usecase.AuthUseCase
import ru.stream.cleantest.domain.auth.usecase.impl.AuthUseCaseImpl
import ru.stream.cleantest.domain.entities.ObfuscateStringEntity
import ru.stream.cleantest.domain.entities.ObfuscateStringEntityImpl

/**
 * Created by Daniil Belevtsev on 08.05.2018 16:19.
 * Project: CleanTest
 */
class DiModule {
    companion object {
        fun provideApi(): Api = MockApiImpl()

        fun provideTokenRepo(): TokenRepository = MockTokenRepositoryImpl()

        fun provideObfuscateStringEntity(): ObfuscateStringEntity = ObfuscateStringEntityImpl()

        fun provideSendAuthEntity(): SendAuthDataEntity = SendAuthDataEntityImpl(provideApi())

        fun provideReceiveEntity(): ReceiveTokenEntity = ReceiveTokenEntityImpl(provideTokenRepo())

        fun provideAuthUseCase(): AuthUseCase {
            return AuthUseCaseImpl(provideReceiveEntity(),
                    provideSendAuthEntity(),
                    provideObfuscateStringEntity())
        }
    }
}